[[!meta date="Fri, 27 Aug 2021 09:09:46 +0300"]]
[[!meta title="Project start"]]

I have in recent time learned about [SSH CA][] as a way to make use of
SSH more convenient, while making it more secure. However, while
managing an SSH CA instance with just the `ssh-keygen` tool is
possible, I want more convenient tooling. This project is about
developing such tooling.

The `sshca` tool will be free and open source software, but I hope to
offer some paid-for training and support to those who can pay.

[[!meta title="sshca - manage an SSH CA"]]

[[Source|https://gitlab.com/larswirzenius/sshca/]] &mdash;
[[Documentation|https://doc.liw.fi/sshca/]] &mdash;
[[Blog]] &mdash;
[[License]]

The `sshca` tool helps manage an SSH Certificate Authority ([SSH CA][]) and
create host and user certificates. Such certificates make using and
administering SSH less tedious and more secure.

An SSH CA is an SSH key dedicated to signing, or certifying, other SSH
keys. Such a signed key is called a certificate and is used together
with the private part of the certified key. The certificate is used
instead of the public key.

SSH clients and servers can be configured to trust certificates made
by one or more CA keys. This makes it possible for a client to trust a
server without asking the user to accept the host key for each new
server. A server can trust a client without having the client's public
key configured for that user in the `authorized_key` file. This
simplifies overall key management significantly, but requires creating
and managing CA keys and certificates.

[SSH CA]: https://liw.fi/sshca

# News

<div class="newslist">
[[!inline pages="page(blog/*)" limit=5 archive=yes trail=no feeds=no]]
</div>
